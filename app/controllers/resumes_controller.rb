class ResumesController < ApplicationController
  DATA = {
    name: 'Teja Sophista Vidyadara Rusyana',
    picture: 'me.jpg',
    phone: '+(62) 857 2039 5525',
    email: 'tejanium@yahoo.com',
    dob: Date.parse('5 February 1989'),
    current_location: 'Jakarta',
    github: 'http://github.com/tejanium',
    marital_status: 'Married',
    personal_traits: 'A passionate, self-motivated programmer who fell in love with Ruby. Loves to write human readable and testable code. Follow best practices, SOLID principles, and applied common OOP patterns. Loves to learn various new things before go to bed.',
    professional_experiences: [
      {
        company: 'PT. Domikado',
        title: 'Head of Product Division',
        start_date: Date.parse('March 2015'),
        end_date: nil,
        description: 'Responsible for hiring and managing people and projects, allocate resources, coaching, and decision making for every technologies and project architectures, including servers, mainly in AWS. Optimizing and maintaining several existing projects. My only goal is to help people to follow coding discipline and best practices. Still heavily involved in coding.'
      }, {
        company: 'PT. Domikado',
        title: 'Web Developer',
        start_date: Date.parse('June 2012'),
        end_date: nil,
        description: 'Involved in internal products, responsible for writing core API and backend using Rails. Duties including maintenance, test, and refactoring code as well as writing new features. Involved in most of the technical decision making.'
      }, {
        company: 'PT. Walden Global Services',
        title: 'Web Developer',
        start_date: Date.parse('June 2011'),
        end_date: Date.parse('May 2011'),
        description: 'Responsible for develop web applications using Rails. Involved in several projects using both Rails 2 and 3, using various technology and library. Duties including requirement specifications, designing database relations, writing code for both server side and client side, writing test, and deploy applications. Direct communication with client, and manage team mate.'
      }, {
        company: 'Telkom Research and Development Center',
        title: 'Intern',
        start_date: Date.parse('January 2010'),
        end_date: Date.parse('April 2010'),
        description: 'Working as a trainee programmer under direct supervision of Telkom senior programmer, responsible for research and building an internet gateway using CodeIgniter on PHP.'
      }
    ],
    personal_experiences: 'Constantly contributes to open-source world by writing gems, reporting bugs and pull requests to other open-source projects. All public activities and repositories hosted and available at Rubygems and Github.',
    technical_skills: {
      languages: ['Ruby', 'JavasScript', 'NodeJS', 'Coffescript', 'Elixir', 'SQL', 'Java', 'Scala', 'PHP'],
      test_frameworks: ['RSpec', 'MiniTest'],
      frameworks: ['Rails', 'jQuery', 'Laravel', 'SailsJS'],
      text_editors: ['Sublime Text', 'vim'],
      databases: ['MySQL', 'PostgreSQL', 'Mongo', 'Redis'],
      version_controls: ['SVN', 'Git']
    },
    educations: [
      {
        name: 'Universitas Pendidikan Indonesia',
        location: 'Bandung',
        year: '2006',
        graduate: '2011',
        major: 'Computer Science'
      }
    ]
  }

  def data
    render json: DATA
  end

  def show
    url = "#{ Rails.application.config_for(:application)['domain'] }/resume"
    @people = People.get(url)
  end
end
