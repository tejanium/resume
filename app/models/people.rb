class People < OpenStruct
  def self.get(url)
    url = URI.parse(url)
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }

    new JSON.parse(res.body)
  end

  def professional_experiences
    @professional_experiences ||= @table[:professional_experiences].map do |professional_experience|
      ProfessionalExperience.new professional_experience
    end
  end

  def educations
    @educations ||= @table[:educations].map do |education|
      Education.new education
    end
  end

  def technical_skills
    @technical_skills ||= TechnicalSkills.new @table[:technical_skills]
  end
end
