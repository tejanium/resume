class ProfessionalExperience < OpenStruct
  def formatted_start_date
    Date.parse(start_date).strftime('%B %Y')
  end

  def formatted_end_date
    return 'current' unless end_date

    Date.parse(end_date).strftime('%B %Y')
  end
end
